var http      = require('http');
var httpProxy = require('http-proxy');


var proxy = new httpProxy.createProxyServer({});

var proxyServer = http.createServer(function (req, res) {
  console.log('req', req);
  proxy.web(req, res, {target: 'http://0.0.0.0:8085'});
});

proxyServer.listen(81, "0.0.0.0");