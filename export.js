const express = require('express')
const app = express()
const port = 3000
const { exec } = require('child_process');
const axios = require('axios');
const params = process.argv.slice(2);

const fs = require('fs')
const path = require('path')
const {NodeSSH} = require('node-ssh')

let api = 'http://162.144.106.252:3000';

let importTo = {
  host: "localhost",
  user: "root",
  password: "lana@123456",
  // database: "lanaapps_test"
}

// let importTo = {
//   host: "localhost",
//   user: "root",
//   password: "123456789",
//   // database: "lanaapps_test"
// }

async function getStoreData() {
  var mysql = require('mysql');
  return new Promise((resolve, reject) => {
    var con = mysql.createConnection(Object.assign(importTo, {
      database : "lanaapps_system"
    }));
    
    con.connect();
    
    con.query(`SELECT * FROM stores WHERE id = ${params[0]} `, function (error, results, fields) {
      if (error) {
        console.log('mysql error', error);
        reject(error);
      };
      // console.log('The solution is: ', results[0]);
      resolve(results[0])
    });
    
    con.end();
  });
}


async function downloadSqlFile(storeData) {
  return new Promise((resolve, reject) => {
    const downloadUrl = api + '/import?dbname=' + storeData.database_name;
    exec(`curl ${downloadUrl} --output ${storeData.database_name}.sql`, (err, stdout, stderr) => {
      if (err) { 
        console.error(`exec error: ${err}`); 
        return reject(err);
      }

      return resolve(true);
    });
  })
}

async function doSql(storeData) {
  return new Promise((resolve, reject) => {
    
    if (!params.includes('--skip-create-db')) {
      var mysql = require('mysql');
      var con = mysql.createConnection(importTo);
      
      con.connect();
      
      con.query(`CREATE DATABASE ${storeData.database_name};`, function (error, results, fields) {
        if (error) { 
          console.error(`exec error: ${error}`); 
          return reject(error);
        }
        console.log('The solution is: ');
      });
      
      con.end();
    }

    console.log('start importing db');
    // const Importer = require('mysql-import');
    // const importer = new Importer(Object.assign(importTo, {
    //   database : storeData.database_name
    // }));

    // // New onProgress method, added in version 5.0!
    // importer.onProgress(progress=>{
    //   var percent = Math.floor(progress.bytes_processed / progress.total_bytes * 10000) / 100;
    //   console.log(`${percent}% Completed`);
    // });

    // importer.setEncoding('utf8');
    // importer.import(`${storeData.database_name}.sql`).then(()=>{
    //   var files_imported = importer.getImported();
    //   console.log(`${files_imported.length} SQL file(s) imported.`);
    //   resolve(true);
    // }).catch(err=>{
    //   console.error(err);
    //   return reject(err);
    // });
    
    exec(`mysql -u${importTo.user} -p${importTo.password} -h ${importTo.host} --default-character-set=utf8  ${storeData.database_name} < ${storeData.database_name}.sql`, (err, stdout, stderr) => {
      if (err) { 
        console.error(`exec error: ${err}`); 
        return reject(err);
      }

      console.log(`The import has finished.`);
      resolve(true);
    });
  })
}

async function unzipDownloadedFolder(storeData, dist) {
  return new Promise((resolve, reject) => {
    const realStoresPath = '/home/lanaapps/public_html/lnasa.space';
    
    exec(`
        sudo unzip ${dist}/${storeData.path}.zip -d ${realStoresPath} && 
        sudo chmod -R 777 ${realStoresPath}/${storeData.path}
      `, {maxBuffer: 1024 * 5000}, (err, stdout, stderr) => {
      if (err) { 
        console.error(`exec unzip error: ${err}`); 
        return reject(err);
      }
      
      console.log(`folder unzipped`);
        
      return resolve(true);
    });
  })
}


async function mvStoreFiles(storeData) {
  
  return new Promise(async (resolve, reject) => {

    
    const dist = path.resolve(__dirname, './stores');
    const target = '/home/lanaapps/public_html/lnasa.space';

    async function zipFolder(ssh) {
      // Command
      return ssh.execCommand(`
        zip -r ${storeData.path}.zip ${storeData.path}
      `, { cwd:target })
      // .then(function(result) {
      //   console.log('STDOUT: ' + result.stdout)
      //   console.log('STDERR: ' + result.stderr)
      // })
    }

    try {

      const ssh = new NodeSSH()
 
      ssh.connect({
        host: '162.144.106.252',
        username: 'root',
        password : ',(V.ft~rSDlC]&@'
        // host : '192.64.113.147',
        // username : 'root',
        // password : 'wXJCx1C08IH54ugvvF30'
      })
      .then(async (res) => {
        
        
        let zipRes = await zipFolder(ssh).then(result => {
          console.log('folder zipped');
          return true;
        })
        .catch(err => {
          console.log('zip err', err)
          return false;
        });

        if (zipRes == true) {
          // Local, Remote
          let downloadZipFolder = await ssh.getFile(`${dist}/${storeData.path}.zip`, `${target}/${storeData.path}.zip`)
          .then((contents, err) => {
            if (err) {
              console.log("Something's wrong")
              console.log(error)
              return false;
            }
            
            console.log("The File's contents were successfully downloaded")
            return true;
          });

          if (downloadZipFolder != true)  {
            return reject('err downloadZipFolder');
          }

          let unzipFolder = await unzipDownloadedFolder(storeData, dist).then(res => {
            console.log('zipping has been done');
            return true;
          })
          .catch(err => {
            console.log('zipping opr err', err);
            return false;
          })

          resolve(true);
        }

      })
    }
    catch(err) {
      reject(err);
    }
  });
}

async function someWaitPlease(time) {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      resolve(true);
    }, time)
  })
}

async function start() {
  try {

    let storeData = await getStoreData();
    if (!storeData) {
      console.log('no store');
      return false;
    }
    console.log('start', storeData);

    await downloadSqlFile(storeData);
    console.log('file ready');
    
    await doSql(storeData);
    console.log('sql file downloaded');

    await mvStoreFiles(storeData);
    console.log('done');

  }
  catch(err) {
    console.log('err', err);
  }
}


start();

// var mysql = require('mysql');
// var con = mysql.createConnection(importTo);
 
// con.connect();
 
// con.query(`CREATE DATABASE ${dbname};`, function (error, results, fields) {
//   if (error) throw error;
//   console.log('The solution is: ');
// });
 
// con.end();

// exec(`curl ${remoteFile} --output ${dumpFile}`, (err, stdout, stderr) => {

//   // New onProgress method, added in version 5.0!
//   exec(`mysql -u${importTo.user} -p${importTo.password} -h${importTo.host} ${dbname} < ${dumpFile}`, (err, stdout, stderr) => {
//     if (err) { console.error(`exec error: ${err}`); return; }

//     console.log(`The import has finished.`);
//   });
// });
