const express = require('express')
const app = express()
const port = 3000
const { exec } = require('child_process');
const fs = require('fs');
const path = require('path');

// Servers
// 162.144.106.252
// 192.64.113.147

// var mysql = require('mysql');
// var con = mysql.createConnection({
//   host: "192.64.113.147",
//   user: "root",
//   password: "lana@123456",
//   database : "lanaapps_system"
// });
 
// con.connect();
 
// con.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//   if (error) throw error;
//   console.log('The solution is: ', results[0].solution);
// });
 
// con.end();


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/export_v2', (req, res) => {

  let dbname = req.query['dbname'];
  let dumpFile = dbname + '.sql';
  let remoteFile = 'http://162.144.106.252:3000/' + dumpFile;

  const host = 'localhost';
  const user = 'root';
  const password = 'lana@123456';
  const database = 'enab_enab';

  const Importer = require('mysql-import');
  const importer = new Importer({host, user, password, database});

  exec(`curl ${remoteFile} --output ${dumpFile}`, (err, stdout, stderr) => {

    // New onProgress method, added in version 5.0!
    importer.onProgress(progress=>{
      var percent = Math.floor(progress.bytes_processed / progress.total_bytes * 10000) / 100;
      console.log(`${percent}% Completed`);
    });

    importer.import(dumpFile).then(()=>{
      var files_imported = importer.getImported();
      console.log(`${files_imported.length} SQL file(s) imported.`);
    }).catch(err=>{
      console.error(err);
    });

  });

  res.send('des')
});

app.get('/export_v1', (req, res) => {

  let dbname = req.query['dbname'];
  let dumpFile = dbname + '.sql';
  let remoteFile = 'http://162.144.106.252:3000/' + dumpFile;

  let importTo = {
    host: "localhost",
    user: "root",
    password: "123456789",
    database: dbname
  }

  // exec(`curl ${remoteFile} --output ${dumpFile}`, (err, stdout, stderr) => {
    console.log('file downloaded', dbname);
    // Import the database.
    exec(`mysql -u ${importTo.user} -p ${importTo.password} -h ${importTo.host} ${dbname} < ${dumpFile}`, (err, stdout, stderr) => {

      if (err) { 
        console.error(`exec error: ${err}`);
      }

      console.log(`The import has finished.`);
    });
  // });

  res.send('working on it');
});

async function doImport(exportFrom, dumpFile, force = null) {
  return new Promise((resolve, reject) => {
    try {

      if (force == 'y' && fs.existsSync('./dbs/' + dumpFile)) {
        resolve(true);
      }

      // Execute a MySQL Dump and redirect the output to the file in dumpFile variable.
      exec(`mysqldump -u${exportFrom.user} -p${exportFrom.password} -h${exportFrom.host} --compact ${exportFrom.database} > dbs/${dumpFile}`, (err, stdout, stderr) => {
        if (err) { 
          console.error(`exec error: ${err}`); 
          return reject(err);
        }
        
        console.log(`Now, importing data to the `);
          
        return resolve(true);
      });
    }
    catch(err) {
      return reject(err);
    }
  })
}

app.get('/import', async (req, res) => {
  let dbname = req.query['dbname'];
  let force = req.query['force'];

  // Where would the file be located?
  let dumpFile = dbname + '.sql';

  let exportFrom = {
    host: "localhost",
    user: "root",
    password: "lana@123456",
    database: dbname
  }

  console.log(`Starting exporting data from the ${exportFrom.database} database`);
  try {
    await doImport(exportFrom, dumpFile, force);

    res.sendFile(path.resolve(__dirname, './dbs/' + dumpFile));
  } catch(err) {
    res.send('error ' + err);
  }
  
})

app.use(express.static('dbs'))

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at http://localhost:${port}`)
})